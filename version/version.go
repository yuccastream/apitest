package version

import "fmt"

var (
	Version  = "0.0.5"
	Revision string
)

type VersionInfo struct {
	Version  string `json:"version"`
	Revision string `json:"revision"`
}

func GetVersion() *VersionInfo {
	return &VersionInfo{
		Version:  Version,
		Revision: Revision,
	}
}

func (v *VersionInfo) String() string {
	if len(v.Revision) == 0 {
		return v.Version
	}
	return fmt.Sprintf("%s (%s)", v.Version, v.Revision)
}
