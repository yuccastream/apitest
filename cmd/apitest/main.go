package main

import (
	"flag"
	"os"

	"gitlab.com/yuccastream/apitest/cmd"
	"gitlab.com/yuccastream/apitest/pkg/apitest"
)

var (
	projectDirFlag  = flag.String("project", "./", "Project directory")
	projectTypeFlag = flag.String("project-type", "basic", "Project type")

	urlFlag       = flag.String("url", "", "The URL to make a request")
	urlMethodFlag = flag.String("method", "GET", "The HTTP method for this request step")
	sourceFlag    = flag.String("source", "", "Assertion source")
	propertyFlag  = flag.String("property", "", "Property")
	tagFilterFlag = flag.String("tag", "", "Filter steps by tag")

	testFlag = flag.Bool("test", false, "Test comparison")
	lintFlag = flag.Bool("lint", false, "Validate test")
)

func main() {
	flag.Parse()
	cmd.ParseCommonFlags()
	at := &apitest.Apitest{
		ProjectDir:    *projectDirFlag,
		ProjectType:   *projectTypeFlag,
		URL:           *urlFlag,
		RequestMethod: *urlMethodFlag,
		Source:        *sourceFlag,
		Property:      *propertyFlag,
	}
	if *testFlag {
		os.Exit(at.HandleTest())
	}
	os.Exit(at.Run(*lintFlag, *tagFilterFlag))
}
