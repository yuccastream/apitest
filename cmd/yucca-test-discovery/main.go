package main

import (
	"flag"
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/yuccastream/apitest/cmd"
	"gitlab.com/yuccastream/apitest/pkg/test-discovery/yucca"
)

var o yucca.Options

func init() {
	flag.StringVar(&o.YuccaAddr, "yucca-addr", "http://testing.yuccastream.com/", "Yucca's address to send API requests")
	flag.StringVar(&o.YuccaUsername, "yucca-username", "", "Yucca's username")
	flag.StringVar(&o.YuccaPassword, "yucca-password", "", "Yucca's password")
	flag.StringVar(&o.TestNamePrefix, "test-name-prefix", "(testing)", "Name prefix for projects")
	flag.StringVar(&o.TestDataDir, "test-data-dir", "./yucca-tests", "Output directory for tests")
	flag.StringVar(&o.TestWebhook, "test-webhook", "http://apitest.yuccastream.com/test", "Webhook address")
	flag.BoolVar(&o.TestTelegramEnable, "test-notification-telegram", true, "Notify by Telegram")
	flag.StringVar(&o.TestTelegramNotifyOn, "test-notification-telegram-notify-on", "switch", "When notification will be sent")
}

func main() {
	flag.Parse()
	cmd.ParseCommonFlags()
	ytd := yucca.NewYuccaTestDiscovery(&o)
	if err := ytd.Run(); err != nil {
		logrus.Error(err)
		os.Exit(1)
	}
}
