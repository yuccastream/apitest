package main

import (
	"flag"
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/yuccastream/apitest/cmd"
	"gitlab.com/yuccastream/apitest/pkg/dashboard"
)

var (
	listenAddressFlag = flag.String("listen-address", ":8810", "Addres to server requests")
	secretFlag        = flag.String("secret", "", "The string is used to verify data")
)

func main() {
	flag.Parse()
	cmd.ParseCommonFlags()
	d, err := dashboard.New(*secretFlag, *listenAddressFlag)
	if err != nil {
		logrus.Error(err)
		os.Exit(1)
	}
	if err := d.Serve(); err != nil {
		logrus.Error(err)
		os.Exit(1)
	}
}
