package cmd

import (
	"flag"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/yuccastream/apitest/version"
)

var (
	versionFlag = flag.Bool("version", false, "Prints version and exit")

	logLevelFlag  = flag.String("log-level", "info", "Logging level")
	logFormatFlag = flag.String("log-format", "text", "Logs format (text or json)")
)

func ParseCommonFlags() {
	setupLogger()
	if *versionFlag {
		printsVersion()
		os.Exit(0)
	}
}

func printsVersion() {
	v := version.GetVersion()
	fmt.Println(v.String())
}

func setupLogger() {
	logrus.SetFormatter(&logrus.TextFormatter{DisableColors: true})
	if *logFormatFlag == "json" {
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}
	logrus.SetOutput(os.Stdout)
	level, err := logrus.ParseLevel(*logLevelFlag)
	if err != nil {
		logrus.Warningf("Unable set '%s' log level", *logLevelFlag)
	} else {
		logrus.SetLevel(level)
	}
}
