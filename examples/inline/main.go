// @test name PFA
// @test description Testing VirtualService rules for staging environment

package main

import "fmt"

// @step note Test route #1
// @step step_type request
// @step method GET
// @step url https://www.w3schools.com/xml/simple.xml
// @step assertion response_status equal 200
// @step assertion response_xml //breakfast_menu/food[1]/name contains Waffles
//
// @step note Test route #2
// @step step_type request
// @step method GET
// @step url https://www.w3schools.com/xml/simple.xml
// @step assertion response_status equal 200
// @step assertion response_xml //breakfast_menu/food[1]/name contains Waffles
func main() {
	fmt.Println("Hello")
}
