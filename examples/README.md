# Examples

## Usage

```bash
$ ./bin/apitest --log-level=debug --project=examples/alpha_filter/
$ ./bin/apitest --log-level=debug --project=examples/alpha_replacers/
$ ./bin/apitest --log-level=debug --project=examples/basic_auth/
$ ./bin/apitest --log-level=debug --project=examples/ffprobe/
$ ./bin/apitest --log-level=debug --project=examples/html/
$ ./bin/apitest --log-level=debug --project=examples/include/
$ ./bin/apitest --log-level=debug --project=examples/json/
$ ./bin/apitest --log-level=debug --project=examples/pause/
$ ./bin/apitest --log-level=debug --project=examples/variables/
$ ./bin/apitest --log-level=debug --project=examples/webhooks/
$ ./bin/apitest --log-level=debug --project=examples/xml/
```
