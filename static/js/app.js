angular.module('myApp', [
    'myApp.controllers',
    'myApp.services',
    'myApp.filters',
    'ngMaterial',
    'ngRoute',
    'ngCookies',
    'ngMaterial',
])

    .factory('authInterceptor', function ($q, $cookies) {
        return {
            request: function(config) {
                config.headers = config.headers || {};
                if ($cookies.get('token')) {
                    config.headers['Authorization'] = 'token '+$cookies.get('token');
                }
                return config;
            },
            responseError: function(rejection) {
                var defer = $q.defer();
                if (rejection.status == 401) {
                    window.location = "#!token";
                }
                defer.reject(rejection);
                return defer.promise;
            }
        };
    })

    .config(function($httpProvider, $routeProvider, $locationProvider) {
        $httpProvider.useApplyAsync(true);
        $httpProvider.interceptors.push('authInterceptor');

        $routeProvider
            .when('/token', {
                templateUrl: 'partials/views/token.html',
                controller: 'TokenController',
            })

            .when('/test/:testId*', {
                templateUrl: 'partials/views/test.html',
                controller: 'TestController',
            })

            .when('/', {
                templateUrl: 'partials/views/tests.html',
                controller: 'TestsController',
            })

            .otherwise('/');
        $locationProvider.html5Mode(false);
        $locationProvider.hashPrefix('!');
    });
