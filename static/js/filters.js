angular.module('myApp.filters', [])

    .filter('safeHtml', function ($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        };
    });
