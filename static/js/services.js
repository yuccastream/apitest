angular.module('myApp.services', [])

    .service('API', function ($http) {
        this.deleteTest = function(name) {
            return $http.delete(
                '/test?name=' + name
            );
        };
        this.getDashboard = function () {
            return $http.get(
                '/tests'
            );
        };
        this.getTest = function (name) {
            return $http.get(
                '/test?name=' + name
            );
        };
    });
