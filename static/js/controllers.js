angular.module('myApp.controllers', [])

    .controller('TokenController', function ($scope, $cookies) {
        $scope.token = "";

        $scope.saveToken = function() {
            var now = new Date();
            $cookies.put("token", $scope.token, {
                path: '/',
                expires: new Date(now.getFullYear()+1, now.getMonth(), now.getDate()),
            })
            window.location = "/";
        }
    })

    .controller('UserController', function ($scope, $cookies) {
        $scope.logout = function() {
            $cookies.remove("token");
            window.location = "/";
        }
    })

    .controller('TestController', function ($scope, $routeParams, API, $mdDialog) {
        $scope.loaded = false;
        $scope.test = {};
        $scope.testId = $routeParams.testId;

        $scope.start = function() {
            GetTest();
        }

        $scope.deleteTest = function(ev) {
            var confirm = $mdDialog.confirm()
                .title('Delete a test')
                .textContent('Are you sure you want to delete test "' + $scope.test.name + '"?')
                .targetEvent(ev)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function() {
                API.deleteTest(window.encodeURIComponent($scope.test.name))
                    .then(function () {
                        window.location = "#!/";
                    });
            }, function() {});
        };

        $scope.start();

        function GetTest() {
            API.getTest($scope.testId)
                .then(function (result) {
                    $scope.test = result.data.response;
                    if ($scope.test.result == "pass") {
                        $scope.test.color = 'lightgreen';
                    } else {
                        $scope.test.color = '#F88';
                    }
                    $scope.loaded = true;
                });
        }
    })

    .controller('TestsController', function ($scope, API) {
        $scope.dashboard = {};
        $scope.loaded = false;

        $scope.start = function() {
            GetDashboard();
        }

        $scope.refresh = function() {
            GetDashboard();
        }

        $scope.start();

        function GetDashboard() {
            API.getDashboard()
                .then(function (result) {
                    dashboard = processDashboard(result.data.response);
                    $scope.dashboard = dashboard;
                    $scope.loaded = true;
                });
        }

        function processDashboard(data) {
            var results = [];
            angular.forEach(data, function(test) {
                var obj = {
                    'url': '#!/test/'+window.encodeURIComponent(test.name),
                    'name': test.name,
                    'description': test.description,
                    'finished_at': test.finished_at,
                };
                if (test.result == "pass") {
                    obj.state = '\u2714';
                    obj.color = 'green';
                } else {
                    obj.state = '\u2716';
                    obj.color = 'red';
                }
                results.push(obj);
            });
            return results;
        }
    });
