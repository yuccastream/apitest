# apitest

Early prototype, do not use.

## Supported actions

* request
* pause
* condition
* ffprobe

## Supported sources

* response_status
* response_headers
* response_size
* response_time
* response_text
* response_json
* response_xml
* response_html

## Supported comparisons

* equal
* not_equal
* empty
* not_empty
* contains
* does_not_contain
* is_a_number
* is_null
* is_less_than
* is_less_than_or_equal
* is_greater_than
* is_greater_than_or_equal
* has_key
* has_value
* in_array
* not_in_array

## Examples

See [examples](examples/) folder.
