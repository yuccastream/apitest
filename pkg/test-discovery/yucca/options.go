package yucca

type Options struct {
	YuccaAddr            string
	YuccaUsername        string
	YuccaPassword        string
	TestNamePrefix       string
	TestDataDir          string
	TestWebhook          string
	TestTelegramEnable   bool
	TestTelegramNotifyOn string
}
