package yucca

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strings"
	"time"

	"gopkg.in/yaml.v2"

	"gitlab.com/yuccastream/apitest/pkg/apitest"
)

type YuccaTestDiscovery struct {
	httpCli *http.Client
	o       *Options
}

type Stream struct {
	ID   string      `json:"id"`
	Name string      `json:"name"`
	Tags []string    `json:"tags"`
	Live *LiveStream `json:"live"`
}

type LiveStream struct {
	IsEnable bool   `json:"isEnable"`
	HLS      string `json:"hls"`
}

func NewYuccaTestDiscovery(options *Options) *YuccaTestDiscovery {
	options.YuccaAddr = strings.TrimSuffix(options.YuccaAddr, "/")
	return &YuccaTestDiscovery{
		o: options,
		httpCli: &http.Client{
			Timeout: 15 * time.Second,
		},
	}
}

func (y *YuccaTestDiscovery) createTestFromStream(stream *Stream) *apitest.Test {
	test := &apitest.Test{}
	if len(y.o.YuccaUsername) > 0 {
		test.Auth = &apitest.Auth{
			Type:     "basic",
			Username: y.o.YuccaUsername,
			Password: y.o.YuccaPassword,
		}
	}
	test.Tags = stream.Tags
	test.Name = stream.Name
	test.Description = fmt.Sprintf("Discovered from %s (Task ID: %s)", y.o.YuccaAddr, stream.ID)
	if len(y.o.TestNamePrefix) > 0 {
		test.Name = y.o.TestNamePrefix + " " + stream.Name
	}
	test.InitialVariables = map[string]string{
		"URL": y.o.YuccaAddr + stream.Live.HLS,
	}
	if len(y.o.TestWebhook) > 0 {
		test.Webhooks = []string{
			y.o.TestWebhook,
		}
	}
	if y.o.TestTelegramEnable {
		test.EnvVariables = []string{
			"APITEST_TELEGRAM_TOKEN",
			"APITEST_TELEGRAM_CHAT_ID",
		}
		test.Notification = &apitest.Notification{
			Telegram: &apitest.Telegram{
				ChatID:   "{{.APITEST_TELEGRAM_CHAT_ID}}",
				Token:    "{{.APITEST_TELEGRAM_TOKEN}}",
				NotifyOn: y.o.TestTelegramNotifyOn,
			},
		}
	}
	test.Steps = []*apitest.Step{}
	ffprobeVar := &apitest.Variable{
		Name: "ffprobe",
	}
	ffprobeVar.DataSource.Source = apitest.DataSourceText
	ffprobeStep := &apitest.Step{
		Note:     "Probe stream",
		StepType: apitest.StepTypeFFprobe,
		Source:   "{{.URL}}",
		Variables: []*apitest.Variable{
			ffprobeVar,
		},
	}
	test.Steps = append(test.Steps, ffprobeStep)
	return test
}

func (y *YuccaTestDiscovery) Run() error {
	streams, err := y.getStreams()
	if err != nil {
		return err
	}
	for _, stream := range streams {
		if stream.Live == nil {
			continue
		}
		if !stream.Live.IsEnable {
			continue
		}
		test := y.createTestFromStream(stream)
		testDir := path.Join(y.o.TestDataDir, stream.Name)
		testFile := path.Join(testDir, ".apitest.yml")
		fmt.Println("#", test.String())
		err := os.MkdirAll(testDir, os.ModePerm)
		if err != nil {
			return err
		}
		b, err := yaml.Marshal(test)
		if err != nil {
			return err
		}
		if err := ioutil.WriteFile(testFile, b, os.ModePerm); err != nil {
			return err
		}
		fmt.Println(string(b))
	}
	return nil
}

func (y *YuccaTestDiscovery) newRequest(route string) (*http.Request, error) {
	req, err := http.NewRequest(http.MethodGet, y.o.YuccaAddr+route, nil)
	if err != nil {
		return nil, err
	}
	if len(y.o.YuccaUsername) > 0 {
		req.SetBasicAuth(y.o.YuccaUsername, y.o.YuccaPassword)
	}
	return req, nil
}

func (y *YuccaTestDiscovery) getStreams() ([]*Stream, error) {
	req, err := y.newRequest("/v1/streams")
	if err != nil {
		return nil, err
	}
	resp, err := y.httpCli.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code: %s", resp.Status)
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	streams := []*Stream{}
	err = json.Unmarshal(b, &streams)
	if err != nil {
		return nil, err
	}
	return streams, nil
}
