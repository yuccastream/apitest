package dashboard

import (
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"

	"gitlab.com/yuccastream/apitest/pkg/apitest"
	"gitlab.com/yuccastream/apitest/version"
)

var (
	ErrIncorrectRequestMethod = errors.New("Incorrect request method")
	ErrEmptyTestName          = errors.New("Test name must be specified")
	ErrTestNotFound           = errors.New("Test not found")
	ErrOperationNotAllowed    = errors.New("Operation not allowed")

	errStatusCodeMap = map[error]int{
		ErrIncorrectRequestMethod: http.StatusMethodNotAllowed,
		ErrEmptyTestName:          http.StatusBadRequest,
		ErrTestNotFound:           http.StatusNotFound,
		ErrOperationNotAllowed:    http.StatusUnauthorized,
	}

	testSuccessVec = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "apitest",
			Name:      "test_success",
			Help:      "Test success",
		}, []string{"name"},
	)
)

type Dashboard struct {
	secret        string
	listenAddress string
	listener      net.Listener
	testSuccess   *prometheus.GaugeVec

	mu    sync.Mutex
	tests map[string]apitest.Test
}

type ApiResponse struct {
	Response   interface{} `json:"response"`
	Status     string      `json:"status"`
	StatusCode int         `json:"status_code"`
	Error      string      `json:"error,omitempty"`
}

type UIAssetWrapper struct {
	FileSystem http.FileSystem
}

func (fs *UIAssetWrapper) Open(name string) (http.File, error) {
	file, err := fs.FileSystem.Open(name)
	if err == nil {
		return file, nil
	}
	if err == os.ErrNotExist {
		return fs.FileSystem.Open("index.html")
	}
	return nil, err
}

func New(secret, listenAddress string) (*Dashboard, error) {
	d := &Dashboard{
		secret:        secret,
		testSuccess:   testSuccessVec,
		listenAddress: listenAddress,
		tests:         make(map[string]apitest.Test),
	}
	err := prometheus.Register(d.testSuccess)
	if err != nil {
		return nil, err
	}
	listener, err := net.Listen("tcp", listenAddress)
	if err != nil {
		return nil, err
	}
	d.listener = listener
	return d, nil
}

func (d *Dashboard) Serve() error {
	logrus.Infof("Starting apitest-dashboard %s...", version.Version)
	logrus.Infof("Serving requests on %s", d.listener.Addr().String())
	http.HandleFunc("/healthy", d.wrap(d.Health, false))
	http.HandleFunc("/tests", d.wrap(d.Tests, true))
	http.HandleFunc("/test", d.wrap(d.Test, true))
	http.HandleFunc("/version", d.wrap(d.Version, false))
	http.Handle("/metrics", promhttp.Handler())
	http.Handle("/", http.FileServer(&UIAssetWrapper{FileSystem()}))

	return http.Serve(d.listener, nil)
}

func (d *Dashboard) IsAccessAllowed(r *http.Request) bool {
	if len(d.secret) == 0 {
		return true
	}
	secret := r.URL.Query().Get("secret")
	if d.secret == secret {
		return true
	}
	secret = r.Header.Get("Authorization")
	return secret == fmt.Sprintf("token %s", d.secret)
}

func (d *Dashboard) wrap(handler func(resp http.ResponseWriter, req *http.Request) (interface{}, error), protected bool) func(resp http.ResponseWriter, req *http.Request) {
	f := func(resp http.ResponseWriter, req *http.Request) {
		encoder := json.NewEncoder(resp)
		reqURL := req.URL.String()
		start := time.Now()
		defer func() {
			logrus.Debugf("Request complete: method=%s, path=%s, duration=%s", req.Method, reqURL, time.Since(start).String())
		}()
		var (
			obj interface{}
			err error
		)
		response := &ApiResponse{
			Response:   []string{},
			StatusCode: http.StatusOK,
		}
		if protected {
			if !d.IsAccessAllowed(req) {
				obj = []string{}
				err = ErrOperationNotAllowed
			} else {
				obj, err = handler(resp, req)
			}
		} else {
			obj, err = handler(resp, req)
		}
		response.Response = obj
		if err != nil {
			response.StatusCode = http.StatusInternalServerError
			code, ok := errStatusCodeMap[err]
			if ok {
				response.StatusCode = code
			}
			response.Error = err.Error()
			logrus.Errorf("Request failed: method=%s, path=%s, error=%v, code=%d", req.Method, reqURL, err, response.StatusCode)
		}
		response.Status = http.StatusText(response.StatusCode)
		resp.WriteHeader(response.StatusCode)
		resp.Header().Set("Content-Type", "application/json")
		err = encoder.Encode(response)
		if err != nil {
			http.Error(resp, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	return f
}

func filterTestsByResult(tests map[string]apitest.Test, resultFilter string) map[string]apitest.Test {
	result := make(map[string]apitest.Test)
	if len(resultFilter) == 0 {
		return tests
	}
	for name, test := range tests {
		switch resultFilter {
		case apitest.ResultFail:
			if test.IsFailed() {
				result[name] = test
			}
		case apitest.ResultPass:
			if test.IsPassed() {
				result[name] = test
			}
		default:
			result[name] = test
		}
	}
	return result
}

func filterTestsByTag(tests map[string]apitest.Test, tagFilter string) map[string]apitest.Test {
	result := make(map[string]apitest.Test)
	if len(tagFilter) == 0 {
		return tests
	}
	for name, test := range tests {
		for _, tag := range test.Tags {
			if tag != tagFilter {
				continue
			}
			result[name] = test
		}
	}
	return result
}

func (d *Dashboard) ListTests(r *http.Request) []apitest.Test {
	var (
		result        []apitest.Test
		filteredTests map[string]apitest.Test
	)
	d.mu.Lock()
	tests := d.tests
	d.mu.Unlock()

	values := r.URL.Query()

	resultFilter := values.Get("result")
	filteredTests = filterTestsByResult(tests, resultFilter)

	tagFilter := values.Get("tag")
	filteredTests = filterTestsByTag(filteredTests, tagFilter)

	for _, test := range filteredTests {
		result = append(result, test)
	}

	return result
}

func (d *Dashboard) GetTest(r *http.Request) (*apitest.Test, error) {
	name := r.URL.Query().Get("name")
	if len(name) == 0 {
		return nil, ErrEmptyTestName
	}
	d.mu.Lock()
	test, ok := d.tests[name]
	d.mu.Unlock()
	if !ok {
		return nil, ErrTestNotFound
	}
	return &test, nil
}

func (d *Dashboard) DeleteTest(r *http.Request) error {
	name := r.URL.Query().Get("name")
	if len(name) == 0 {
		return ErrEmptyTestName
	}
	d.mu.Lock()
	delete(d.tests, name)
	d.mu.Unlock()
	return nil
}

func (d *Dashboard) AddTest(r *http.Request) error {
	var test apitest.Test
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&test); err != nil {
		return err
	}
	if len(test.Name) == 0 {
		return ErrEmptyTestName
	}
	logrus.Infof("Test '%s' received", test.Name)
	testResult := 0.0
	if test.IsPassed() {
		testResult = 1.0
	}
	d.testSuccess.WithLabelValues(test.Name).Set(testResult)
	test.FlushCriticalData()
	d.mu.Lock()
	d.tests[test.Name] = test
	d.mu.Unlock()
	return nil
}
