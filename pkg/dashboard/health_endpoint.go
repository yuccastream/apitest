package dashboard

import (
	"net/http"
)

func (d *Dashboard) Health(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	return "ok", nil
}
