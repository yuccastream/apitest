package dashboard

import (
	"net/http"

	"gitlab.com/yuccastream/apitest/version"
)

func (d *Dashboard) Version(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	switch r.Method {
	case http.MethodGet:
		return version.GetVersion(), nil
	default:
		return nil, ErrIncorrectRequestMethod
	}
}
