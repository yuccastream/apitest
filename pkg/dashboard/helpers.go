package dashboard

import (
	"sort"

	"gitlab.com/yuccastream/apitest/pkg/apitest"
)

func SortTests(s []apitest.Test) {
	sort.Slice(s, func(i, j int) bool {
		a, b := s[i], s[j]
		time1, time2 := a.FinishedAt, b.FinishedAt
		return time1.After(time2)
	})
}
