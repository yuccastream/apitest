package dashboard

import (
	"net/http"
)

func (d *Dashboard) Test(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	switch r.Method {
	case http.MethodPost:
		err := d.AddTest(r)
		if err != nil {
			return nil, err
		}
		return "success", nil
	case http.MethodDelete:
		err := d.DeleteTest(r)
		if err != nil {
			return nil, err
		}
		return "success", nil
	case http.MethodGet:
		test, err := d.GetTest(r)
		return test, err
	default:
		return nil, ErrIncorrectRequestMethod
	}
}
