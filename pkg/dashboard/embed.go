package dashboard

import (
	"io/fs"
	"log"
	"net/http"

	"gitlab.com/yuccastream/apitest"
)

func FileSystem() http.FileSystem {
	fsys, err := fs.Sub(apitest.DistFS, "static")
	if err != nil {
		log.Fatal(err.Error())
	}
	return http.FS(fsys)
}
