package dashboard

import (
	"net/http"

	"gitlab.com/yuccastream/apitest/pkg/apitest"
)

func (d *Dashboard) Tests(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	switch r.Method {
	case http.MethodGet:
		tests := d.ListTests(r)
		if len(tests) == 0 {
			tests = []apitest.Test{}
		}
		SortTests(tests)
		return tests, nil
	default:
		return nil, ErrIncorrectRequestMethod
	}
}
