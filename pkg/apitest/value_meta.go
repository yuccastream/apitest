package apitest

import (
	"fmt"
	"html"
	"strconv"
	"strings"
)

type ValueMeta struct {
	Source    string
	Value     string
	IsNumeric bool
	Type      string
	FloatVal  float64
	Array     []string
}

func (v *ValueMeta) ShortValue() string {
	limit := 100
	text := html.EscapeString(v.Value)
	if len(text) > limit {
		return fmt.Sprintf("%s…", strings.TrimSpace(text[0:limit]))
	}
	return text
}

func (v *ValueMeta) RefreshFloatValue() {
	if !IsNumeric(v.Value) {
		return
	}
	v.IsNumeric = true
	i, _ := strconv.ParseFloat(v.Value, 64)
	v.FloatVal = i
}

func GetValueMetaByString(value string) ValueMeta {
	valueMeta := ValueMeta{
		Value:     value,
		IsNumeric: IsNumeric(value),
	}
	if valueMeta.IsNumeric {
		i, _ := strconv.ParseFloat(value, 64)
		valueMeta.FloatVal = i
	}
	return valueMeta
}
