package apitest

import (
	"errors"
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
)

type Comparison struct {
	ctype       string
	checkedMeta ValueMeta
	userMeta    ValueMeta
}

var (
	deprecatedAssertionComparison = map[string]string{
		"numeric":               "is_a_number",
		"less_than":             "is_less_than",
		"less_than_or_equal":    "is_less_than_or_equal",
		"greater_than":          "is_greater_than",
		"greater_than_or_equal": "is_greater_than_or_equal",
	}
)

func NewComparison(ctype string, checkedMeta, userMeta ValueMeta) *Comparison {
	return &Comparison{
		ctype:       ctype,
		checkedMeta: checkedMeta,
		userMeta:    userMeta,
	}
}

func (c *Comparison) ShowDeprecatedWarn() {
	if newName, ok := deprecatedAssertionComparison[c.ctype]; ok {
		logrus.Warnf("Assertion comparison '%s' is deprecated, use '%s' instead", c.ctype, newName)
	}
}

func (c *Comparison) Check() (bool, error) {
	c.ShowDeprecatedWarn()
	switch c.ctype {
	case "equal":
		return c.Equal()
	case "not_equal":
		return c.NotEqual()
	case "empty":
		return c.IsEmpty()
	case "not_empty":
		return c.NotEmpty()
	case "contains":
		return c.Contains()
	case "does_not_contain":
		return c.DoesNotContains()
	case "numeric", "is_a_number":
		// TODO(leo): Rename to is_a_number in 0.1.0
		return c.IsANumber()
	case "is_null":
		return c.IsNull()
	case "less_than", "is_less_than":
		// TODO(leo): Rename to is_less_than in 0.1.0
		return c.IsLessThan()
	case "less_than_or_equal", "is_less_than_or_equal":
		// TODO(leo): Rename to is_less_than_or_equal in 0.1.0
		return c.IsLessThanOrEqual()
	case "greater_than", "is_greater_than":
		// TODO(leo): Rename to is_greater_than in 0.1.0
		return c.IsGreaterThan()
	case "greater_than_or_equal", "is_greater_than_or_equal":
		// TODO(leo): Rename to is_greater_than_or_equal in 0.1.0
		return c.IsGreaterThanOrEqual()
	case "has_key":
		// Key already checked in GetValueMetaFromResponse
		return true, nil
	case "has_value":
		return c.HasValue()
	case "in_array":
		return c.InArray()
	case "not_in_array":
		return c.NotInArray()
	default:
		return false, fmt.Errorf("unknown comparison: %s", c.ctype)
	}
}

func (c *Comparison) HasValue() (bool, error) {
	for _, value := range c.checkedMeta.Array {
		if c.userMeta.Value == strings.TrimSpace(value) {
			return true, nil
		}
	}
	return false, fmt.Errorf("tested array [%s] does not have a value '%s'", strings.Join(c.checkedMeta.Array, ", "), c.userMeta.ShortValue())
}

func (c *Comparison) InArray() (bool, error) {
	userArray := strings.Split(c.userMeta.Value, ",")
	for _, v := range userArray {
		if c.checkedMeta.Value == strings.TrimSpace(v) {
			return true, nil
		}
	}
	return false, fmt.Errorf("tested value '%s' not in array [%s]", c.checkedMeta.ShortValue(), c.userMeta.ShortValue())
}

func (c *Comparison) NotInArray() (bool, error) {
	userArray := strings.Split(c.userMeta.Value, ",")
	for _, v := range userArray {
		if c.checkedMeta.Value == strings.TrimSpace(v) {
			return false, fmt.Errorf("tested value '%s' in array [%s]", c.checkedMeta.ShortValue(), c.userMeta.ShortValue())
		}
	}
	return true, nil
}

func (c *Comparison) IsGreaterThan() (bool, error) {
	if !c.checkedMeta.IsNumeric {
		return false, fmt.Errorf("tested value '%s' is not a number", c.checkedMeta.ShortValue())
	}
	if !c.userMeta.IsNumeric {
		return false, fmt.Errorf("value from assertion '%s' is not a number", c.checkedMeta.ShortValue())
	}
	if c.checkedMeta.FloatVal <= c.userMeta.FloatVal {
		return false, fmt.Errorf("tested value '%s' not greater than value from assertion '%s'", c.checkedMeta.ShortValue(), c.userMeta.ShortValue())
	}
	return true, nil
}

func (c *Comparison) IsGreaterThanOrEqual() (bool, error) {
	if !c.checkedMeta.IsNumeric {
		return false, fmt.Errorf("tested value '%s' is not a number", c.checkedMeta.ShortValue())
	}
	if !c.userMeta.IsNumeric {
		return false, fmt.Errorf("value from assertion '%s' is not a number", c.checkedMeta.ShortValue())
	}
	if c.checkedMeta.FloatVal < c.userMeta.FloatVal {
		return false, fmt.Errorf("tested value '%s' not equal to or greater than value from assertion '%s'", c.checkedMeta.ShortValue(), c.userMeta.ShortValue())
	}
	return true, nil
}

func (c *Comparison) IsLessThan() (bool, error) {
	if !c.checkedMeta.IsNumeric {
		return false, fmt.Errorf("tested value '%s' is not a number", c.checkedMeta.ShortValue())
	}
	if !c.userMeta.IsNumeric {
		return false, fmt.Errorf("value from assertion '%s' is not a number", c.checkedMeta.ShortValue())
	}
	if c.checkedMeta.FloatVal >= c.userMeta.FloatVal {
		return false, fmt.Errorf("tested value '%s' not less than value from assertion '%s'", c.checkedMeta.ShortValue(), c.userMeta.ShortValue())
	}
	return true, nil
}

func (c *Comparison) IsLessThanOrEqual() (bool, error) {
	if !c.checkedMeta.IsNumeric {
		return false, fmt.Errorf("tested value '%s' is not a number", c.checkedMeta.ShortValue())
	}
	if !c.userMeta.IsNumeric {
		return false, fmt.Errorf("value from assertion '%s' is not a number", c.checkedMeta.ShortValue())
	}
	if c.checkedMeta.FloatVal > c.userMeta.FloatVal {
		return false, fmt.Errorf("tested value '%s' not equal to or less than value from assertion '%s'", c.checkedMeta.ShortValue(), c.userMeta.ShortValue())
	}
	return true, nil
}

func (c *Comparison) Equal() (bool, error) {
	if c.checkedMeta.Value != c.userMeta.Value {
		return false, fmt.Errorf("tested value '%s' not equal to value from assertion '%s'", c.checkedMeta.ShortValue(), c.userMeta.ShortValue())
	}
	return true, nil
}

func (c *Comparison) NotEqual() (bool, error) {
	if c.checkedMeta.Value == c.userMeta.Value {
		return false, fmt.Errorf("tested value '%s' is equal to value from assertion '%s'", c.checkedMeta.ShortValue(), c.userMeta.ShortValue())
	}
	return true, nil
}

func (c *Comparison) IsEmpty() (bool, error) {
	if len(c.checkedMeta.Value) != 0 {
		return false, fmt.Errorf("tested value '%s' is not empty", c.checkedMeta.ShortValue())
	}
	return true, nil
}

func (c *Comparison) NotEmpty() (bool, error) {
	if len(c.checkedMeta.Value) == 0 {
		return false, errors.New("tested value is empty")
	}
	return true, nil
}

func (c *Comparison) Contains() (bool, error) {
	if !strings.Contains(c.checkedMeta.Value, c.userMeta.Value) {
		return false, fmt.Errorf("tested value '%s' is not contains value from assertion '%s'", c.checkedMeta.ShortValue(), c.userMeta.ShortValue())
	}
	return true, nil
}

func (c *Comparison) DoesNotContains() (bool, error) {
	if strings.Contains(c.checkedMeta.Value, c.userMeta.Value) {
		return false, fmt.Errorf("tested value '%s' is contains value from assertion '%s'", c.checkedMeta.ShortValue(), c.userMeta.ShortValue())
	}
	return true, nil
}

func (c *Comparison) IsANumber() (bool, error) {
	if !c.checkedMeta.IsNumeric {
		return false, fmt.Errorf("tested value '%s' is not a number", c.checkedMeta.ShortValue())
	}
	return true, nil
}

func (c *Comparison) IsNull() (bool, error) {
	if c.checkedMeta.Type != "null" {
		return false, fmt.Errorf("tested value '%s' is not null", c.checkedMeta.ShortValue())
	}
	return true, nil
}
