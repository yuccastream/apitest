package apitest

import (
	"errors"
)

const (
	NotificationOnAll      = "all"
	NotificationOnFailures = "failures"
	NotificationOnSwitch   = "switch"
)

type Notification struct {
	Telegram *Telegram `json:"telegram,omitempty" yaml:"telegram,omitempty"`
	tpl      *Template
}

func (n *Notification) SetTemplate(tpl *Template) {
	n.tpl = tpl
}

func (n *Notification) Send(test *Test) error {
	message := test.GetNotificationMessage()
	if len(message) == 0 {
		return errors.New("empty message text")
	}
	if n.Telegram != nil {
		n.Telegram.ChatID = n.tpl.Do(n.Telegram.ChatID)
		n.Telegram.Token = n.tpl.Do(n.Telegram.Token)
		if len(n.Telegram.NotifyOn) == 0 {
			n.Telegram.NotifyOn = NotificationOnFailures
		}

		switch n.Telegram.NotifyOn {
		case NotificationOnAll:
			return n.Telegram.Send(message)
		case NotificationOnFailures:
			if test.IsFailed() {
				return n.Telegram.Send(message)
			}
		case NotificationOnSwitch:
			if !test.SameResultWithPrevious() {
				return n.Telegram.Send(message)
			}
		}
	}
	return nil
}
