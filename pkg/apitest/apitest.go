package apitest

import (
	"errors"
	"fmt"
	"path"
	"strings"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/yuccastream/apitest/version"
)

type Apitest struct {
	ProjectDir    string
	ProjectType   string
	URL           string
	RequestMethod string
	Source        string
	Property      string
}

func (a *Apitest) ValidateTest() error {
	if len(a.URL) == 0 {
		return errors.New("URL must be defined")
	}
	if len(a.RequestMethod) == 0 {
		return errors.New("request method must be defined")
	}
	if len(a.Source) == 0 {
		return errors.New("source must be defined")
	}
	return nil
}

func (a *Apitest) RunTest(dataSource DataSource) (*ValueMeta, error) {
	client := NewClient()
	r, err := client.NewRequest(a.RequestMethod, a.URL, "")
	if err != nil {
		return nil, err
	}
	resp, err := client.Do(r)
	if err != nil {
		return nil, err
	}
	valueMeta, err := dataSource.GetValueMetaFromResponse(resp)
	if err != nil {
		return nil, err
	}
	return &valueMeta, nil
}

func (a *Apitest) HandleTest() int {
	dataSource := DataSource{
		Source:   a.Source,
		Property: a.Property,
	}
	err := a.ValidateTest()
	if err != nil {
		fmt.Println(err)
		return 1
	}
	valueMeta, err := a.RunTest(dataSource)
	if err != nil {
		fmt.Println(err)
		return 1
	}
	fmt.Printf("Source: %s\n", dataSource.String())
	fmt.Printf("Value: %s\n", valueMeta.Value)
	fmt.Printf("Numeric: %v\n", valueMeta.IsNumeric)
	return 0
}

func (a *Apitest) Run(lintOnly bool, tagFilter string) int {
	var (
		test *Test
		err  error
	)

	projectType := strings.ToLower(a.ProjectType)
	switch projectType {
	case "basic":
		projectFile := path.Join(a.ProjectDir, ".apitest.yml")
		test, err = NewTestFromBasicFile(projectFile)
		if err != nil {
			logrus.Error(err)
			return 1
		}
	case "inline":
		test, err = NewTestFromInlineFile(a.ProjectDir)
		if err != nil {
			logrus.Error(err)
			return 1
		}
		logrus.Infof("%#v", test)
		for _, step := range test.Steps {
			logrus.Infof("%#v", step)
		}
	default:
		logrus.Errorf("Unknown project type: %v", projectType)
		return 1
	}

	if lintOnly {
		fmt.Println("OK")
		return 0
	}

	stateFile := path.Join(a.ProjectDir, ".apitest.state")
	db, err := StateOpen(stateFile)
	if err != nil {
		logrus.Error(err)
		return 1
	}

	prevResult, err := db.Get()
	if err != nil {
		logrus.Error(err)
	}

	logrus.Infof("Starting apitest %s...", version.Version)
	logrus.Info(test.String())

	test.SetTagFilter(tagFilter)
	test.SetPreviousResult(prevResult)
	test.RunWithRetry(time.Second)

	if err := db.Set(test); err != nil {
		logrus.Error(err)
	}

	if err := db.Close(); err != nil {
		logrus.Error(err)
	}

	if test.IsFailed() {
		return 1
	}
	return 0
}
