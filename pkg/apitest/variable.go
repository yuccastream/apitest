package apitest

import "fmt"

type Variable struct {
	DataSource `yaml:",inline"`
	Name       string `json:"name" yaml:"name"`
}

func (v *Variable) Normalize() {
	v.DataSource.Normalize()
}

func (v *Variable) String() string {
	return fmt.Sprintf("%s (From: %s)", v.Name, v.DataSource.String())
}
