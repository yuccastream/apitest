package apitest

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

const (
	StepTypeRequest   = "request"
	StepTypePause     = "pause"
	StepTypeCondition = "condition"
	StepTypeFFprobe   = "ffprobe"
)

var (
	// TODO(leo): Move to client
	supportedHTTPMethods = map[string]bool{
		http.MethodGet:     true,
		http.MethodHead:    true,
		http.MethodPost:    true,
		http.MethodPut:     true,
		http.MethodPatch:   true,
		http.MethodDelete:  true,
		http.MethodOptions: true,
	}
)

type Step struct {
	Note     string   `json:"note" yaml:"note"`
	Tags     []string `json:"tags,omitempty" yaml:"tags"`
	StepType string   `json:"step_type" yaml:"step_type"`
	Include  string   `json:"include,omitempty" yaml:"include,omitempty"`
	// StepTypeCondition
	Comparison string  `json:"comparison,omitempty" yaml:"comparison,omitempty"`
	LeftValue  string  `json:"left_value,omitempty" yaml:"left_value,omitempty"`
	RightValue string  `json:"right_value,omitempty" yaml:"right_value,omitempty"`
	Steps      []*Step `json:"steps,omitempty" yaml:"steps,omitempty"`
	// StepTypeRequest
	Method            string              `json:"method,omitempty" yaml:"method,omitempty"`
	URL               string              `json:"url,omitempty" yaml:"url,omitempty"`
	Headers           map[string][]string `json:"headers,omitempty" yaml:"headers,omitempty"`
	Assertions        []*Assertion        `json:"-" yaml:"assertions,omitempty"`
	AssertionsSummary AssertionsSummary   `json:"assertions,omitempty" yaml:"-"`
	Auth              *Auth               `json:"auth,omitempty" yaml:"auth,omitempty"`
	Variables         []*Variable         `json:"variables,omitempty" yaml:"variables,omitempty"`
	Body              string              `json:"body,omitempty" yaml:"body,omitempty"`
	// StepTypeFFprobe
	Args    []string      `json:"args,omitempty" yaml:"args,omitempty"`
	Source  string        `json:"source,omitempty" yaml:"source,omitempty"`
	Timeout time.Duration `json:"timeout,omitempty" yaml:"timeout,omitempty"`
	// TODO(leo): Add Cookies for `request` step_type
	// TODO(leo): Add VariablesSummary for `request` step_type
	// TODO(leo): Add Form (map[string][]string) for `request` step_type
	// TODO(leo): Add `inbound` step_type:
	// – response for a request is 200 OK
	// – request has timeout
	// – variables
	// – assertions
	// StepTypePause
	Duration time.Duration `json:"duration,omitempty" yaml:"duration,omitempty"`
	// Result
	Result     string    `json:"result" yaml:"-"`
	StartedAt  time.Time `json:"started_at" yaml:"-"`
	FinishedAt time.Time `json:"finished_at" yaml:"-"`
	// Private
	err error
	tpl *Template
}

type AssertionsSummary struct {
	Fail  int `json:"fail"`
	Total int `json:"total"`
	Pass  int `json:"pass"`
}

func (s *Step) FlushSummary() {
	s.AssertionsSummary = AssertionsSummary{
		Fail:  0,
		Total: 0,
		Pass:  0,
	}
}

func (s *Step) ContainsTag(tag string) bool {
	if len(tag) == 0 {
		return true
	}
	for _, t := range s.Tags {
		if t == tag {
			return true
		}
	}
	return false
}

func (s *Step) ReCalcSummary() {
	s.FlushSummary()
	s.CalcSummary()
}

func (s *Step) CalcSummary() {
	for _, assert := range s.Assertions {
		s.AssertionsSummary.Total++
		if assert.IsPassed() {
			s.AssertionsSummary.Pass++
		} else {
			s.AssertionsSummary.Fail++
		}
	}
}

func (s *Step) SetTemplate(tpl *Template) {
	s.tpl = tpl
}

func (s *Step) Error() error {
	return s.err
}

func (s *Step) Fail(err error) {
	s.ReCalcSummary()
	if err != nil {
		logrus.Infof("Fail: %s: %v", s.String(), err)
	} else {
		logrus.Infof("Fail: %s", s.String())
	}
	s.err = err
	s.FinishedAt = time.Now().UTC()
	s.Result = ResultFail
}

func (s *Step) Pass() {
	s.ReCalcSummary()
	logrus.Infof("Pass: %s", s.String())
	s.FinishedAt = time.Now().UTC()
	s.Result = ResultPass
}

func (s *Step) Normalize() {
	if s.Timeout == 0 {
		s.Timeout = 10 * time.Minute
	}
	if len(s.StepType) > 0 {
		s.StepType = strings.ToLower(s.StepType)
	}
	if len(s.Method) > 0 {
		s.Method = strings.ToUpper(s.Method)
	}
	if s.IsRequest() && len(s.Method) == 0 {
		s.Method = http.MethodGet
	}
	if len(s.Assertions) > 0 {
		for _, assertion := range s.Assertions {
			assertion.Normalize()
		}
	}
	if s.IsCondition() {
		s.Comparison = strings.ToLower(s.Comparison)
	}
}

func (s *Step) String() string {
	switch s.StepType {
	case StepTypeRequest:
		if s.HasNote() {
			return fmt.Sprintf("Step: %s (%s %s)", s.Note, s.Method, s.URL)
		}
		return fmt.Sprintf("Step (%s %s)", s.Method, s.URL)
	case StepTypePause:
		if s.HasNote() {
			return fmt.Sprintf("Step: %s (Pause)", s.Note)
		}
		return "Step (Pause)"
	case StepTypeCondition:
		return fmt.Sprintf("Step: Condition: '%s' %s '%s'", s.LeftValue, s.Comparison, s.RightValue)
	case StepTypeFFprobe:
		if s.HasNote() {
			return fmt.Sprintf("Step: %s (ffprobe %s)", s.Note, s.Source)
		}
		return fmt.Sprintf("Step (ffprobe %s)", s.URL)
	default:
		return ""
	}
}

func (s *Step) UpdateRequest(r *http.Request) {
	for name, values := range s.Headers {
		for _, value := range values {
			headerName := s.tpl.Do(name)
			headerValue := s.tpl.Do(value)
			if strings.ToLower(headerName) == "host" {
				r.Host = headerValue
			}
			r.Header.Add(headerName, headerValue)
		}
	}
	if s.Auth != nil && s.Auth.IsBasicAuth() {
		r.SetBasicAuth(s.tpl.Do(s.Auth.Username), s.tpl.Do(s.Auth.Password))
	}
}

func (s *Step) HasNote() bool {
	return len(s.Note) > 0
}

func (s *Step) IsRequest() bool {
	return s.StepType == StepTypeRequest
}

func (s *Step) IsPause() bool {
	return s.StepType == StepTypePause
}

func (s *Step) IsCondition() bool {
	return s.StepType == StepTypeCondition
}

func (s *Step) IsFFprobe() bool {
	return s.StepType == StepTypeFFprobe
}

func (s *Step) Validate() (bool, error) {
	// TODO(leo): Validate variables
	switch s.StepType {
	case StepTypeRequest:
		return s.ValidateAsRequest()
	case StepTypePause:
		return s.ValidateAsPause()
	case StepTypeCondition:
		return s.ValidateAsCondition()
	case StepTypeFFprobe:
		return s.ValidateAsFFprobe()
	default:
		return false, fmt.Errorf("unknown step type: %s", s.StepType)
	}
}

func (s *Step) CheckRequestMethod() bool {
	if _, ok := supportedHTTPMethods[s.Method]; !ok {
		return false
	}
	return true
}

func (s *Step) ValidateAsFFprobe() (bool, error) {
	if len(s.Source) == 0 {
		return false, errors.New("media source must be specified")
	}
	// Check if does not contain variables
	if !strings.Contains(s.Source, "{{") && strings.Contains(s.Source, "://") {
		if _, err := url.Parse(s.Source); err != nil {
			return false, fmt.Errorf("error with parsing ffprobe source as url: %v", err)
		}
	}
	return true, nil
}

func (s *Step) ValidateAsCondition() (bool, error) {
	if len(s.Comparison) == 0 {
		return false, errors.New("comparison must be specified")
	}
	if len(s.RightValue) == 0 {
		return false, errors.New("right value must be specified")
	}
	return true, nil
}

func (s *Step) ValidateAsRequest() (bool, error) {
	if !s.CheckRequestMethod() {
		return false, fmt.Errorf("unknown request method: %s", s.Method)
	}
	if len(s.URL) == 0 {
		return false, errors.New("request url must be specified")
	}
	// Check if does not contain variables
	if !strings.Contains(s.URL, "{{") {
		if _, err := url.Parse(s.URL); err != nil {
			return false, fmt.Errorf("error with parsing request url: %v", err)
		}
	}
	return true, nil
}

func (s *Step) ValidateAsPause() (bool, error) {
	if s.Duration.Seconds() < 0.001 {
		return false, errors.New("duration must be greater than 1 millisecond")
	}
	if s.Duration.Minutes() > 15 {
		return false, errors.New("duration must be less than 15 minutes")
	}
	return true, nil
}

func (s *Step) GetInjectedVariables(resp *Response) map[string]string {
	result := map[string]string{}
	if len(s.Variables) == 0 {
		return result
	}
	for _, variable := range s.Variables {
		variable.Normalize()
		v, err := variable.DataSource.GetValueMetaFromResponse(resp)
		if err != nil {
			logrus.Warnf("Error injecting variable '%s': %v", variable.Name, err)
		} else {
			logrus.Debugf("Inject variable: %s = %s", variable.String(), v.Value)
			result[variable.Name] = v.Value
		}
	}
	return result
}
