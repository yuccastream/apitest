package apitest

import (
	"bytes"
	"io/ioutil"
	"net"
	"net/http"
	"time"

	"gitlab.com/yuccastream/apitest/version"
)

type Client struct {
	client    *http.Client
	DebugHTTP bool
}

type Response struct {
	Body         []byte
	Headers      http.Header
	StatusCode   int
	ResponseTime float64
	// Certificate  *Certificate
}

func NewClient() *Client {
	// https://stackoverflow.com/questions/12122159/golang-how-to-do-a-https-request-with-bad-certificate
	httpTransport := &http.Transport{
		Proxy:              http.ProxyFromEnvironment,
		DisableKeepAlives:  false,
		DisableCompression: false,
		Dial: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 30 * time.Second,
		// TLSClientConfig:     &tls.Config{InsecureSkipVerify: true},
	}

	httpClient := &http.Client{}
	httpClient.Transport = httpTransport
	httpClient.Timeout = 60 * time.Second

	return &Client{
		client: httpClient,
	}
}

func (c *Client) Do(req *http.Request) (*Response, error) {
	startTime := time.Now()
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	endTime := time.Since(startTime).Seconds()
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	// for _, chain := range resp.TLS.VerifiedChains {
	// 	for _, cert := range chain {
	// 		if err := cert.VerifyHostname("hostname.ru"); err == nil {
	// 			fmt.Println(cert.Subject.CommonName)
	// 			fmt.Println(cert.SerialNumber)
	// 			fmt.Println(cert.DNSNames)
	// 			fmt.Println(cert.SignatureAlgorithm.String())
	// 			fmt.Println(cert.Issuer.CommonName)
	// 			fmt.Println(int64(cert.NotAfter.Sub(time.Now()).Hours()))
	// 			fmt.Println("\n")
	// 		}
	// 	}
	// }
	r := &Response{
		Body:         body,
		Headers:      resp.Header,
		StatusCode:   resp.StatusCode,
		ResponseTime: float64(endTime),
	}
	return r, err
}

func (c *Client) NewRequest(method, urlStr, body string) (*http.Request, error) {
	req, err := http.NewRequest(method, urlStr, bytes.NewBufferString(body))
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", "apitest/"+version.Version)
	return req, nil
}
