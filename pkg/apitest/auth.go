package apitest

import "strings"

type Auth struct {
	// TODO(leo): Add client_certificate
	// TODO(leo): Add oAuth
	Type     string `json:"auth_type" yaml:"auth_type"`
	Username string `json:"username" yaml:"username"`
	Password string `json:"password" yaml:"password"`
}

func (a *Auth) IsBasicAuth() bool {
	return strings.ToLower(a.Type) == "basic"
}

func (a *Auth) Protect() {
	a.Username = "***"
	a.Password = "***"
}
