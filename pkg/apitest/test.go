package apitest

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"time"

	yaml "gopkg.in/yaml.v2"

	"github.com/sirupsen/logrus"
)

var (
	DefaultFFprobeArgs = []string{
		"-v",
		"fatal",
		"-analyzeduration",
		"30000000",
		"-probesize",
		"30000000",
		"-print_format",
		"json",
		"-show_format",
		"-show_streams",
	}
)

type Test struct {
	// Environment options:
	// – InitialVariables
	// – Auth
	// – Headers
	// – Emails
	// – Webhooks
	// – Notification
	Name             string              `json:"name" yaml:"name"`
	Tags             []string            `json:"tags,omitempty" yaml:"tags"`
	Description      string              `json:"description" yaml:"description"`
	Steps            []*Step             `json:"steps" yaml:"steps"`
	RetryOnFailure   bool                `json:"retry_on_failure" yaml:"retry_on_failure"`
	StopOnFailure    bool                `json:"stop_on_failure" yaml:"stop_on_failure"`
	Headers          map[string][]string `json:"headers,omitempty" yaml:"headers,omitempty"`
	Auth             *Auth               `json:"auth,omitempty" yaml:"auth,omitempty"`
	Notification     *Notification       `json:"notification,omitempty" yaml:"notification,omitempty"`
	InitialVariables map[string]string   `json:"initial_variables,omitempty" yaml:"initial_variables,omitempty"`
	EnvVariables     []string            `json:"env_variables,omitempty" yaml:"env_variables,omitempty"`
	Variables        map[string]string   `json:"variables,omitempty" yaml:"-"`
	Webhooks         []string            `json:"webhooks,omitempty" yaml:"webhooks"`
	StartedAt        time.Time           `json:"started_at" yaml:"-"`
	FinishedAt       time.Time           `json:"finished_at" yaml:"-"`
	Result           string              `json:"result" yaml:"-"`
	// TODO(leo): Add client_certificate
	// TODO(leo): Add preserve_cookies
	// TODO(leo): Upgrade Notification to Integrations
	// TODO(leo): Add emails (notify_on, notify_threshold, notify_all, recipients[0].email, recipients[0].name)
	tpl            *Template
	previousResult *Test
	tagFilter      string
}

func NewTestFromInlineFile(filename string) (*Test, error) {
	fileBytes, err := ioutil.ReadFile(filename)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, fmt.Errorf("file %s does not exist", filename)
		}
		if os.IsPermission(err) {
			return nil, fmt.Errorf("can't read file %s, check permission", filename)
		}
		return nil, err
	}
	test := &Test{}
	scanner := bufio.NewScanner(bytes.NewReader(fileBytes))
	prevLine := ""
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) == 0 {
			continue
		}
		test.parseInlineString(line, prevLine)
		prevLine = line
	}
	return test, nil
}

func NewTestFromBasicFile(filename string) (*Test, error) {
	var test *Test
	configBytes, err := ioutil.ReadFile(filename)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, fmt.Errorf("file %s does not exist", filename)
		}
		if os.IsPermission(err) {
			return nil, fmt.Errorf("can't read file %s, check permission", filename)
		}
		return nil, err
	}
	if err = yaml.Unmarshal(configBytes, &test); err != nil {
		return nil, err
	}
	test.Variables = map[string]string{}
	for name, value := range test.InitialVariables {
		test.Variables[name] = value
	}
	for _, env := range test.EnvVariables {
		test.Variables[env] = os.Getenv(env)
	}
	tpl := InitWithVariables(test.Variables)
	test.SetTemplate(tpl)
	test.Steps, err = LoadIncludedSteps(path.Dir(filename), test.Steps)
	if err != nil {
		return nil, err
	}
	test.Name = test.tpl.Do(test.Name)
	test.Description = test.tpl.Do(test.Description)

	if test.Notification != nil {
		test.Notification.SetTemplate(tpl)
	}

	if len(test.Steps) == 0 {
		return nil, errors.New("nothing to test, empty steps list")
	}

	for _, step := range test.Steps {
		step.SetTemplate(tpl)
		step.Normalize()
		if step.HasNote() {
			step.Note = test.tpl.Do(step.Note)
		}
		for _, assert := range step.Assertions {
			assert.SetTemplate(tpl)
		}
		if ok, err := step.Validate(); !ok {
			return nil, fmt.Errorf("check step configuration: %v", err)
		}
	}
	if len(test.Webhooks) >= 0 {
		if err := test.ValidateWebhooks(); err != nil {
			return nil, err
		}
	}
	return test, nil
}

func LoadIncludedSteps(dir string, steps []*Step) ([]*Step, error) {
	var stepsResult []*Step
	for _, step := range steps {
		if len(step.Include) == 0 {
			// Save full step from main file
			stepsResult = append(stepsResult, step)
			continue
		}
		files, err := filepath.Glob(path.Join(dir, step.Include))
		if err != nil {
			return nil, fmt.Errorf("path error: %v", err)
		}
		for _, filename := range files {
			stepFromFile := &Step{}
			stepFileBytes, err := ioutil.ReadFile(filename)
			if err != nil {
				return nil, fmt.Errorf("file '%s' has read error: %v", filename, err)
			}
			if err = yaml.Unmarshal(stepFileBytes, &stepFromFile); err != nil {
				return nil, fmt.Errorf("step from file '%s' has unmarshal error: %v", filename, err)
			}
			stepFromFile.Tags = append(stepFromFile.Tags, step.Tags...)
			stepsResult = append(stepsResult, stepFromFile)
		}
	}
	return stepsResult, nil
}

func (t *Test) parseInlineString(line string, prevLine string) {
	if !strings.Contains(line, "@test") && !strings.Contains(line, "@step") {
		return
	}
}

func (t *Test) SameResultWithPrevious() bool {
	if t.previousResult == nil {
		return false
	}
	if t.Result != t.previousResult.Result {
		return false
	}
	return true
}

func (t *Test) IsPassed() bool {
	return t.Result == ResultPass
}

func (t *Test) IsFailed() bool {
	return t.Result == ResultFail
}

func (t *Test) String() string {
	var (
		description string
		name        string
	)
	name = t.Name
	description = t.Description
	if len(name) == 0 {
		name = "noname"
	}
	if len(t.Tags) > 0 {
		if len(description) > 0 {
			description += "; "
		}
		description = fmt.Sprintf("%stags: %s", description, strings.Join(t.Tags, ", "))
	}
	if len(description) > 0 {
		return fmt.Sprintf("%s (%s)", name, description)
	}
	return name
}

func (t *Test) FlushCriticalData() {
	t.Webhooks = []string{}
	t.Notification = nil
}

func (t *Test) ValidateWebhooks() error {
	for _, webhook := range t.Webhooks {
		_, err := url.Parse(webhook)
		if err != nil {
			return fmt.Errorf("webhook error: %v", err)
		}
	}
	return nil
}

func (t *Test) GetFirstAssertationFailure() error {
	for _, step := range t.Steps {
		for _, assert := range step.Assertions {
			if assert.Error() == nil {
				continue
			}
			return fmt.Errorf(
				"%s\n%s\nAssertion: %s\nError: %v",
				t.Name,
				step.String(),
				assert.String(),
				assert.Error(),
			)
		}
	}
	return nil
}

func (t *Test) GetNotificationMessage() string {
	switch t.Result {
	case ResultFail:
		err := t.GetFirstAssertationFailure()
		if err != nil {
			return err.Error()
		}
		// Failed but no one assertion was affected
		return fmt.Sprintf("%s failed", t.Name)
	case ResultPass:
		return fmt.Sprintf("%s passed", t.Name)
	}
	return fmt.Sprintf("%s in unknown state", t.Name)
}

func (t *Test) Fail() {
	t.FinishedAt = time.Now().UTC()
	t.Result = ResultFail
	logrus.Info("Result: Fail")
	if t.Notification == nil {
		return
	}
	if err := t.Notification.Send(t); err != nil {
		logrus.Error(err)
	}
}

func (t *Test) Pass() {
	t.FinishedAt = time.Now().UTC()
	t.Result = ResultPass
	logrus.Info("Result: Pass")
	if t.Notification == nil {
		return
	}
	if err := t.Notification.Send(t); err != nil {
		logrus.Error(err)
	}
}

func (t *Test) SetTemplate(tpl *Template) {
	t.tpl = tpl
}

func (t *Test) SetPreviousResult(test *Test) {
	t.previousResult = test
}

func (t *Test) SetTagFilter(tag string) {
	t.tagFilter = tag
}

func (t *Test) UpdateRequest(r *http.Request) {
	for name, values := range t.Headers {
		for _, value := range values {
			headerName := t.tpl.Do(name)
			headerValue := t.tpl.Do(value)
			if strings.ToLower(headerName) == "host" {
				r.Host = headerValue
			}
			r.Header.Add(headerName, headerValue)
		}
	}
	if t.Auth != nil && t.Auth.IsBasicAuth() {
		r.SetBasicAuth(t.tpl.Do(t.Auth.Username), t.tpl.Do(t.Auth.Password))
	}
}

func (t *Test) MergeVariables(varsA, varsB map[string]string) map[string]string {
	for name, value := range varsB {
		varsA[name] = value
	}
	return varsA
}

func (t *Test) RunWithRetry(delay time.Duration) bool {
	var reRuned bool
ReRunTest:
	ok := t.Run()
	if !ok {
		t.Fail()
		if t.RetryOnFailure && !reRuned {
			logrus.Info("Retrying test...")
			reRuned = true
			time.Sleep(delay)
			goto ReRunTest
		}
	} else {
		t.Pass()
	}
	if len(t.Webhooks) != 0 {
		if err := t.RunWebhooks(); err != nil {
			logrus.Error(err)
		}
	}
	return ok
}

func (t *Test) prepareFFprobeSource(source string, stepAuth *Auth) string {
	u, err := url.Parse(source)
	if err != nil {
		return source
	}
	if t.Auth != nil {
		if t.Auth.IsBasicAuth() {
			user := t.tpl.Do(t.Auth.Username)
			password := t.tpl.Do(t.Auth.Password)
			u.User = url.UserPassword(user, password)
		}
	}
	if stepAuth != nil {
		if stepAuth.IsBasicAuth() {
			user := t.tpl.Do(stepAuth.Username)
			password := t.tpl.Do(stepAuth.Password)
			u.User = url.UserPassword(user, password)
		}
	}
	return u.String()
}

func (t *Test) runStepTypeFFprobe(step *Step) (ok bool, fails int) {
	ffprobe, err := exec.LookPath("ffprobe")
	if err != nil {
		step.Fail(err)
		return false, 0
	}
	step.Source = t.tpl.Do(step.Source)
	args := DefaultFFprobeArgs
	if len(step.Args) > 0 {
		args = step.Args
	}
	source := t.prepareFFprobeSource(step.Source, step.Auth)
	args = append(args, []string{"-i", source}...)
	startTime := time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), step.Timeout)
	defer cancel()
	b, err := exec.CommandContext(ctx, ffprobe, args...).CombinedOutput()
	if ctx.Err() == context.DeadlineExceeded {
		step.Fail(errors.New("timed out"))
		return false, 0
	}
	if err != nil {
		step.Fail(err)
		return false, 0
	}
	endTime := time.Since(startTime).Seconds()
	resp := &Response{
		Body:         b,
		ResponseTime: endTime,
	}
	vars := step.GetInjectedVariables(resp)
	t.Variables = t.MergeVariables(t.Variables, vars)
	for _, assert := range step.Assertions {
		ok, err := assert.Check(resp)
		if ok {
			assert.Pass()
			continue
		}
		assert.Fail(err)
		fails++
		if t.StopOnFailure {
			step.Fail(nil)
			return false, fails
		}
	}
	if fails > 0 {
		step.Fail(nil)
		return false, fails
	}
	step.Pass()
	return true, 0
}

func (t *Test) runStepTypeRequest(step *Step) (ok bool, fails int) {
	client := NewClient()
	step.URL = t.tpl.Do(step.URL)
	r, err := client.NewRequest(step.Method, step.URL, step.Body)
	if err != nil {
		step.Fail(err)
		return false, 0
	}
	t.UpdateRequest(r)
	step.UpdateRequest(r)
	resp, err := client.Do(r)
	if err != nil {
		step.Fail(err)
		return false, 0
	}
	vars := step.GetInjectedVariables(resp)
	t.Variables = t.MergeVariables(t.Variables, vars)
	for _, assert := range step.Assertions {
		ok, err := assert.Check(resp)
		if ok {
			assert.Pass()
			continue
		}
		assert.Fail(err)
		fails++
		if t.StopOnFailure {
			step.Fail(nil)
			return false, fails
		}
	}
	if fails > 0 {
		step.Fail(nil)
		return false, fails
	}
	step.Pass()
	return true, 0
}

func (t *Test) Run() bool {
	t.StartedAt = time.Now().UTC()
	var failsCounter int
	for _, step := range t.Steps {
		if !step.ContainsTag(t.tagFilter) {
			continue
		}
		step.StartedAt = time.Now().UTC()
		var stepFailCounter int
		logrus.Debug(step.String())
		switch step.StepType {
		case StepTypeFFprobe:
			ok, fails := t.runStepTypeFFprobe(step)
			stepFailCounter += fails
			if !ok {
				failsCounter++
			}
		case StepTypeRequest:
			ok, fails := t.runStepTypeRequest(step)
			stepFailCounter += fails
			if !ok {
				failsCounter++
			}
		case StepTypeCondition:
			step.LeftValue = t.tpl.Do(step.LeftValue)
			step.RightValue = t.tpl.Do(step.RightValue)
			leftValueMeta := GetValueMetaByString(step.LeftValue)
			rightValueMeta := GetValueMetaByString(step.RightValue)
			c := NewComparison(step.Comparison, leftValueMeta, rightValueMeta)
			ok, err := c.Check()
			if !ok {
				if len(step.Steps) == 0 {
					step.Fail(err)
					return false
				}
				step.Fail(err)
				continue
			}
			step.Pass()
		case StepTypePause:
			time.Sleep(step.Duration)
			step.Pass()
			logrus.Debugf("Sleep for a %s - OK", step.Duration.String())
		}
	}
	return failsCounter == 0
}

func (t *Test) RunWebhooks() error {
	client := NewClient()
	jsonBody, err := json.Marshal(t)
	if err != nil {
		return fmt.Errorf("error marshaling test: %v", err)
	}
	for _, webhook := range t.Webhooks {
		url := t.tpl.Do(webhook)
		logrus.Debugf("Sending POST request to '%s'...", url)
		req, err := client.NewRequest(http.MethodPost, url, string(jsonBody))
		if err != nil {
			logrus.Errorf("Error with creating request '%s': %v", url, err)
			continue
		}
		req.Header.Set("Content-Type", "application/json")
		if _, err := client.Do(req); err != nil {
			logrus.Errorf("Error with sending request '%s': %v", url, err)
			continue
		}
		logrus.Infof("Request to '%s' has sended", url)
	}
	return nil
}
