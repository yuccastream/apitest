package apitest

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
)

type Assertion struct {
	DataSource `yaml:",inline"`
	Comparison string `json:"comparison" yaml:"comparison"`
	Value      string `json:"value,omitempty" yaml:"value,omitempty"`
	Result     string `json:"result" yaml:"-"`
	err        error
	tpl        *Template
}

func (a *Assertion) Error() error {
	return a.err
}

func (a *Assertion) IsFailed() bool {
	return a.Result == ResultFail
}

func (a *Assertion) IsPassed() bool {
	return a.Result == ResultPass
}

func (a *Assertion) Fail(err error) {
	logrus.Debugf("Fail: %s: %v", a.String(), err)
	a.err = err
	a.Result = ResultFail
}

func (a *Assertion) Pass() {
	logrus.Debugf("Pass: %s", a.String())
	a.Result = ResultPass
}

func (a *Assertion) SetTemplate(tpl *Template) {
	a.tpl = tpl
}

func (a *Assertion) Normalize() {
	a.DataSource.Normalize()
	a.Comparison = strings.ToLower(a.Comparison)
}

func (a *Assertion) String() string {
	sourceStr := a.DataSource.String()
	if len(sourceStr) == 0 {
		return ""
	}
	if len(a.Value) > 0 {
		vm := GetValueMetaByString(a.Value)
		return fmt.Sprintf("%s %s '%s'", a.DataSource.String(), a.Comparison, vm.ShortValue())
	}
	return fmt.Sprintf("%s %s", a.DataSource.String(), a.Comparison)
}

func (a *Assertion) GetUserValueMeta() (ValueMeta, error) {
	userMeta := ValueMeta{
		Value:     a.Value,
		IsNumeric: IsNumeric(a.Value),
	}
	if userMeta.IsNumeric {
		i, _ := strconv.ParseFloat(a.Value, 64)
		userMeta.FloatVal = i
	}
	return userMeta, nil
}

func (a *Assertion) Check(r *Response) (bool, error) {
	a.Property = a.tpl.Do(a.Property)
	a.Value = a.tpl.Do(a.Value)
	userMeta, err := a.GetUserValueMeta()
	if err != nil {
		return false, err
	}
	checkedMeta, err := a.DataSource.GetValueMetaFromResponse(r)
	if err != nil {
		return false, err
	}
	comp := NewComparison(a.Comparison, checkedMeta, userMeta)
	return comp.Check()
}
