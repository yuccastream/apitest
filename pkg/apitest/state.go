package apitest

import (
	"encoding/json"
	"fmt"

	"go.etcd.io/bbolt"
)

var (
	stateBucket = []byte(`state`)
	latestState = []byte(`latest`)
)

type State struct {
	db *bbolt.DB
}

func StateOpen(path string) (*State, error) {
	db, err := bbolt.Open(path, 0600, nil)
	if err != nil {
		return nil, err
	}
	err = db.Update(func(tx *bbolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(stateBucket)
		if err != nil {
			return fmt.Errorf("failed to create state bucket: %s", err)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	s := &State{db}
	return s, nil
}

func (s *State) Set(test *Test) error {
	return s.db.Update(func(tx *bbolt.Tx) error {
		b := tx.Bucket(stateBucket)
		buf, err := json.Marshal(test)
		if err != nil {
			return err
		}
		return b.Put(latestState, buf)
	})
}

func (s *State) Get() (*Test, error) {
	var test *Test
	err := s.db.View(func(tx *bbolt.Tx) error {
		b := tx.Bucket(stateBucket)
		v := b.Get(latestState)
		if len(v) == 0 {
			return nil
		}
		return json.Unmarshal(v, &test)
	})
	if err != nil {
		return nil, err
	}
	return test, nil
}

func (s *State) Close() error {
	return s.db.Close()
}
