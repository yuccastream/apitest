package apitest

import (
	"bytes"
	"text/template"

	"github.com/sirupsen/logrus"
)

type Template struct {
	variables map[string]string
}

func InitWithVariables(vars map[string]string) *Template {
	return &Template{
		variables: vars,
	}
}

func (t *Template) Add(key, value string) {
	t.variables[key] = value
}

func (t *Template) Check(text string) error {
	_, err := template.New("action").Option("missingkey=zero").Parse(text)
	return err
}

func (t *Template) Do(text string) string {
	var tpl bytes.Buffer
	tmpl, err := template.New("action").Option("missingkey=zero").Parse(text)
	if err != nil {
		logrus.Warnf("Template '%s' parse error: %v", text, err)
		return ""
	}
	if err := tmpl.Execute(&tpl, t.variables); err != nil {
		logrus.Warnf("Template '%s' execute error: %v", text, err)
		return ""
	}
	return tpl.String()
}
