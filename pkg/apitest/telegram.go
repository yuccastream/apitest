package apitest

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Telegram struct {
	// TODO(leo): Add notify_on (all, failures, threshold, switch)
	ChatID   string `json:"chat_id" yaml:"chat_id"`
	Token    string `json:"token" yaml:"token"`
	NotifyOn string `json:"notify_on" yaml:"notify_on"`
}

func (t *Telegram) Send(message string) error {
	url := fmt.Sprintf("https://api.telegram.org/bot%s/sendMessage", t.Token)
	postData := make(map[string]interface{})
	postData["chat_id"] = t.ChatID
	postData["text"] = message
	postData["parse_mode"] = "HTML"
	postData["disable_web_page_preview"] = true
	var post bytes.Buffer
	enc := json.NewEncoder(&post)
	err := enc.Encode(postData)
	if err != nil {
		return err
	}
	resp, err := http.Post(url, "application/json", &post)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		type response struct {
			Description string `json:"description"`
			ErrorCode   int    `json:"error_code"`
			Ok          bool   `json:"ok"`
		}
		res := &response{}
		err = json.Unmarshal(body, res)
		if err != nil {
			return fmt.Errorf("failed to understand Telegram response (err: %s). url: %s data: %v code: %d content: %s", err.Error(), url, &postData, resp.StatusCode, string(body))
		}
		return fmt.Errorf("SendMessage error (%d) description: %s", res.ErrorCode, res.Description)
	}
	return nil
}
