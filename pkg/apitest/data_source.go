package apitest

import (
	"bytes"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"github.com/tidwall/gjson"
	"gopkg.in/xmlpath.v2"
)

const (
	DataSourceJSON    = "response_json"
	DataSourceStatus  = "response_status"
	DataSourceHeaders = "response_headers"
	DataSourceText    = "response_text"
	DataSourceSize    = "response_size"
	DataSourceTime    = "response_time"
	DataSourceXML     = "response_xml"
	DataSourceHTML    = "response_html"
	// TODO(leo): Add response_cookies
)

var (
	DataSourcePrefixes = map[string]string{
		DataSourceJSON:    "Response JSON body field",
		DataSourceStatus:  "HTTP status code",
		DataSourceHeaders: "Response header",
		DataSourceText:    "Response text",
		DataSourceSize:    "Response size",
		DataSourceTime:    "Response time",
		DataSourceXML:     "Response XML body element/attribute",
		DataSourceHTML:    "Response HTML body element/attribute",
	}
)

type DataSource struct {
	Source         string     `json:"source" yaml:"source"`
	Property       string     `json:"property,omitempty" yaml:"property,omitempty"`
	AlphaFilter    string     `json:"alpha_filter,omitempty" yaml:"alpha_filter,omitempty"`
	AlphaReplacers [][]string `json:"alpha_replacers,omitempty" yaml:"alpha_replacers,omitempty"`
}

func (d *DataSource) Normalize() {
	d.Source = strings.ToLower(d.Source)
}

func (d *DataSource) String() string {
	prefix, ok := DataSourcePrefixes[d.Source]
	if !ok {
		return ""
	}
	if len(d.Property) == 0 {
		return prefix
	}
	return fmt.Sprintf("%s '%s'", prefix, d.Property)
}

func (d *DataSource) GetSourceValueMetaForJSON(r *Response) (ValueMeta, error) {
	var sourceValueMeta ValueMeta
	v := gjson.Get(string(r.Body), d.Property)
	if !v.Exists() {
		return sourceValueMeta, fmt.Errorf("JSON key '%s' not found", d.Property)
	}
	sourceValueMeta = ValueMeta{
		Source:    d.Source,
		Value:     v.String(),
		IsNumeric: IsNumeric(v.String()),
		Type:      strings.ToLower(v.Type.String()),
	}
	if sourceValueMeta.IsNumeric {
		sourceValueMeta.FloatVal = v.Float()
	}
	arrayValues := v.Array()
	if len(arrayValues) != 0 {
		for _, arrayValue := range arrayValues {
			sourceValueMeta.Array = append(sourceValueMeta.Array, arrayValue.String())
		}
	}
	return sourceValueMeta, nil
}

func (d *DataSource) GetSourceValueMetaForStatus(r *Response) (ValueMeta, error) {
	v := strconv.Itoa(r.StatusCode)
	sourceValueMeta := GetValueMetaByString(v)
	sourceValueMeta.Source = d.Source
	return sourceValueMeta, nil
}

func (d *DataSource) GetSourceValueMetaForHeaders(r *Response) (ValueMeta, error) {
	v := r.Headers.Get(d.Property)
	sourceValueMeta := GetValueMetaByString(v)
	sourceValueMeta.Source = d.Source
	return sourceValueMeta, nil
}

func (d *DataSource) GetSourceValueMetaForText(r *Response) (ValueMeta, error) {
	v := string(r.Body)
	sourceValueMeta := GetValueMetaByString(v)
	sourceValueMeta.Source = d.Source
	return sourceValueMeta, nil
}

func (d *DataSource) GetSourceValueMetaForSize(r *Response) (ValueMeta, error) {
	v := strconv.Itoa(len(r.Body))
	sourceValueMeta := GetValueMetaByString(v)
	sourceValueMeta.Source = d.Source
	return sourceValueMeta, nil
}

func (d *DataSource) GetSourceValueMetaForTime(r *Response) (ValueMeta, error) {
	v := fmt.Sprintf("%f", r.ResponseTime)
	sourceValueMeta := GetValueMetaByString(v)
	sourceValueMeta.Source = d.Source
	return sourceValueMeta, nil
}

// TODO(leo): Add has_value support
func (d *DataSource) GetSourceValueMetaForXML(r *Response) (ValueMeta, error) {
	var sourceValueMeta ValueMeta
	var elementCounter int
	root, err := xmlpath.ParseHTML(bytes.NewReader(r.Body))
	if err != nil {
		return sourceValueMeta, fmt.Errorf("error parsing body: %v", err)
	}
	if strings.HasSuffix(d.Property, "/#") {
		property := d.Property[0 : len(d.Property)-2]
		xpath, err := xmlpath.Compile(property)
		if err != nil {
			return sourceValueMeta, fmt.Errorf("error parsing property: %v", err)
		}
		iter := xpath.Iter(root)
		for iter.Next() {
			elementCounter++
		}
		v := strconv.Itoa(elementCounter)
		sourceValueMeta = GetValueMetaByString(v)
		sourceValueMeta.Source = d.Source
	} else {
		xpath, err := xmlpath.Compile(d.Property)
		if err != nil {
			return sourceValueMeta, fmt.Errorf("error parsing property: %v", err)
		}
		v, ok := xpath.String(root)
		if !ok {
			return sourceValueMeta, fmt.Errorf("path '%s' not found", d.Property)
		}
		sourceValueMeta = GetValueMetaByString(v)
		sourceValueMeta.Source = d.Source
	}
	return sourceValueMeta, nil
}

func (d *DataSource) PostProcessValue(valueMeta *ValueMeta) {
	prevValue := valueMeta.Value
	valueMeta.Value = d.FilterValue(valueMeta.Value)
	valueMeta.Value = d.ReplaceValue(valueMeta.Value)
	if prevValue != valueMeta.Value {
		valueMeta.RefreshFloatValue()
	}
}

func (d *DataSource) ReplaceValue(value string) string {
	if len(d.AlphaReplacers) == 0 {
		return value
	}
	for _, replacer := range d.AlphaReplacers {
		if len(replacer) < 2 {
			continue
		}
		value = strings.Replace(value, replacer[0], replacer[1], -1)
	}
	return value
}

func (d *DataSource) FilterValue(value string) string {
	if len(d.AlphaFilter) == 0 {
		return value
	}
	re, err := regexp.Compile(d.AlphaFilter)
	if err != nil {
		return value
	}
	value = re.FindString(value)
	return value
}

func (d *DataSource) GetValueMetaFromResponse(r *Response) (ValueMeta, error) {
	var v ValueMeta
	var err error
	switch d.Source {
	case DataSourceJSON:
		v, err = d.GetSourceValueMetaForJSON(r)
	case DataSourceStatus:
		v, err = d.GetSourceValueMetaForStatus(r)
	case DataSourceHeaders:
		v, err = d.GetSourceValueMetaForHeaders(r)
	case DataSourceText:
		v, err = d.GetSourceValueMetaForText(r)
	case DataSourceSize:
		v, err = d.GetSourceValueMetaForSize(r)
	case DataSourceTime:
		v, err = d.GetSourceValueMetaForTime(r)
	case DataSourceXML, DataSourceHTML:
		v, err = d.GetSourceValueMetaForXML(r)
	default:
		err = fmt.Errorf("unknown source type: %s", d.Source)
	}
	if err != nil {
		return v, err
	}
	d.PostProcessValue(&v)
	return v, nil
}
