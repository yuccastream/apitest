GIT_COMMIT := $(shell git rev-parse HEAD)
GIT_DIRTY := $(if $(shell git status --porcelain),+CHANGES)
GO_LDFLAGS := "-X gitlab.com/yuccastream/apitest/version.Revision=$(GIT_COMMIT)$(GIT_DIRTY)"

.PHONY: build
build: linux

.PHONY: build-apitest
build-apitest:
	@echo "--> Compiling apitest"
	@mkdir -p bin/
	@GOOS=linux GOARCH=amd64 go build -o bin/apitest -v ./cmd/apitest

.PHONY: build-dashboard
build-dashboard: static-assets
	@echo "--> Compiling dashboard"
	@mkdir -p bin/
	@GOOS=linux GOARCH=amd64 go build -ldflags $(GO_LDFLAGS) -o bin/dashboard -v ./cmd/dashboard

.PHONY: local
local: static-assets
	@echo "--> Compiling apitest and dashboard"
	@go build -o bin/apitest -v ./cmd/apitest
	@go build -ldflags $(GO_LDFLAGS) -tags $(GO_TAGS) -o bin/dashboard -v ./cmd/dashboard

.PHONY: linux
linux: build-apitest build-dashboard
